import { Component, EventEmitter, Output, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CardComponent implements OnInit {
  @Input() value;
  @Input() show;
  @Output() visiable = new EventEmitter();

  showValue() {
    if(this.show)
      return this.value;
  }

  constructor() { }

  ngOnInit() {
  }

}
