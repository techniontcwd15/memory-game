import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  cardsValues = [1,1,2,2,3,3,4,4];
  cardsVisable = [false, false,false,false,false,false,false,false];
}
